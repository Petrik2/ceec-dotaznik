
import bootstrap from 'bootstrap';
import Popper from "popper.js";


import '../sass/main.scss';

var $ = require("jquery");
window.jQuery = $;


$(document).ready(function () {
    var inputField = $(".input-group").find("input[type=text], input[type=number]");


    //pridá checked na radio/checkbox pri click na vedľajší text/number input
    inputField.click(function () {
        var RadioOrBox = $(this).parent().siblings().find("input[type=radio], input[type=checkbox]");
        RadioOrBox.prop("checked", true);
    });


});